module close_line(w, l, t=0.4, s=0.4, h=0.3){
    n=l/(t+s);
    o=t+s;
    
    for(i=[0:n]){
        translate([i*o, 0, 0])
        cube([t, w, h], center=true);
    }
}

module close_ring(r_in, r_out, t=0.4, s=0.4, h=0.3){
    l=PI*r_in*2;
    n=l/(t+s);
    w=r_out-r_in;
    r=(r_in+r_out)/2;
    a=360/n;
    
    for(i=[0:n-1]){
        rotate([0, 0, i*a])
        translate([0, r, 0])
        cube([t, w, h], center=true);
    }
}

module print_cooler(
    ext_height=12, thickness=2,
    fan_ext_size=60, fan_int_size=56,
    fan_bolt_diam=3.4, fan_bolt_dist=50,
    fan_offset=80, fan_bevel=2,
    
    bridge_size=40, bridge_channels=8, bridge_edge=1,
    
    out_ext_size=60, out_int_size=44,
    out_bolt_diam=3, out_bolt_dist=50,
    
    hole_height=4,
    hole_ring_diam=44, hole_thickness=2
){
    fan_size=fan_ext_size-fan_bevel*2;
    fan_ext_height=ext_height;
    fan_int_height=ext_height-thickness;
    
    /* bridge */
    bridge_length=fan_offset;
    bridge_int_length=fan_offset-(fan_int_size+out_int_size-fan_bevel*8)/2;
    bridge_elements=bridge_channels;
    bridge_wall_tickness=bridge_edge;
    bridge_height=ext_height-thickness*2;
    bridge_width=bridge_size; //min(fan_int_size, out_ext_size-thickness*2);
    bridge_element_width=(bridge_width-(bridge_elements-1)*bridge_wall_tickness)/bridge_elements;

    out_size=fan_offset+(fan_ext_size+out_ext_size)/2-fan_bevel*2;
    out_ext_height=ext_height;
    out_int_height=ext_height-hole_height-thickness;
    
    difference(){
        translate([-fan_offset, 0, 0])
        minkowski(){
            translate([-fan_size/2, -fan_size/2, 0])
            cube([out_size, fan_size, fan_ext_height/2]);
            cylinder(r=fan_bevel, h=fan_ext_height/2);
        }
        
        /* fan-side */
        translate([-fan_offset, 0, thickness])
        intersection(){
            cylinder(r=fan_int_size/2, h=fan_int_height+$fix);
            translate([fan_ext_size/2, 0, fan_ext_height])
            scale([1, 1, fan_ext_height/fan_int_size])
            sphere(r=fan_ext_size);
        }
        
        translate([-fan_offset, 0, -$fix])
        for(angle=[0, 90, 180, 270]){
            rotate([0, 0, angle])
            translate([fan_bolt_dist/2, fan_bolt_dist/2, 0])
            cylinder(r=fan_bolt_diam/2, h=fan_ext_height+$fix*2);
        }
        
        /* out-side */
        difference(){
            union(){
                translate([-bridge_length, -bridge_width/2, thickness])
                for(element=[0:bridge_elements-1]){
                    translate([0, (bridge_element_width+bridge_wall_tickness)*element, 0])
                    cube([bridge_length, bridge_element_width, bridge_height-0.3]);
                }
                
                *translate([-out_ext_size/2-bridge_int_length/2, 0, bridge_height+thickness-0.15])
                close_line(bridge_width, bridge_int_length/2, 1, 1, 0.3+$fix);
                
                translate([0, 0, hole_height])
                cylinder(r=out_ext_size/2-thickness, h=out_int_height);
                
                *translate([0, 0, hole_height+out_int_height+0.15])
                close_ring(out_int_size/2+thickness, out_ext_size/2-thickness, 1, 1, 0.3+$fix);
                
                translate([0, 0, -$fix])
                cylinder(r1=(hole_ring_diam+hole_thickness)/2, r2=out_ext_size/2-thickness, h=hole_height+$fix*2);
            }
            
            translate([0, 0, hole_height-$fix])
            cylinder(r=out_int_size/2+thickness, h=out_int_height+$fix*2);
            
            translate([0, 0, -$fix*2])
            cylinder(r1=(hole_ring_diam-hole_thickness)/2, r2=out_int_size/2+thickness, h=hole_height+$fix*4);
        }
        
        translate([0, 0, hole_height-$fix])
        cylinder(r=out_int_size/2, h=out_ext_height+$fix*2);
        
        translate([0, 0, -$fix*2])
        cylinder(r1=hole_ring_diam/2-thickness, r2=out_int_size/2, h=hole_height+$fix*4);
        
        for(angle=[0, 90, 180, 270]){
            rotate([0, 0, angle])
            translate([out_bolt_dist/2, out_bolt_dist/2, -$fix])
            cylinder(r=out_bolt_diam/2, h=out_ext_height+$fix*2);
        }
    }
}

use <../PrusaMendel/source/x-carriage.scad>;

module design(){
    difference(){
        print_cooler();
        translate([-500, 0, -500])
        cube([1000, 1000, 1000]);
    }
    translate([0, 0, 35])
    rotate([0, 180, 180])
    color([1, 1, 1, 0.1])
    xcarriage();
}

module print(){
    print_cooler();
}

design();

//print();

$fn=100;
$fix=0.01;
